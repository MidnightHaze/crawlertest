﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleCrawl.Balancing
{
    public static class PlayerStat
    {
        public static float ExpBase = 10f; //기본 경험치통
        public static float ExpMult = 1.6f; //렙업당 배율

        //렙업당 고정 스탯증가 + 배율 스탯 증가
        public static float LVUP_HP_Offset = 5f;
        public static float LVUP_HP_Multiplier = 1.1f;

        public static float LVUP_STR_Offset = 1f;
        public static float LVUP_STR_Multiplier = 1.05f;

        public static float LVUP_DEF_Offset = 0f;
        public static float LVUP_DEF_Multiplier = 1.1f;

        public static float LVUP_LUK_Offset = 0f;
        public static float LVUP_LUK_Multiplier = 1.1f;
    }
    public static class WeaponParameter
    {
        public static float DagCritMlt = 2.0f;  //단검 추가크리 배율
        public static float DagLukBonus = 1f;//단검 Luck 비례 공격력 추가

        public static float CbwLukBonus = 0.5f;//석궁 Luck비례 공격력 추가

        public static float FlailCritMlt = 1.5f;  //철퇴 추가크리 배율

        public static float AxeStrBonus = 1.5f;//도끼 Str비례 공격력 추가

        public static float BasicStrBonus = 1f;//기본 Str비례 공격력 
    }
}