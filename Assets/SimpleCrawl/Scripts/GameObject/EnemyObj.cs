﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * All enemies of the game should use this call or a class that extend it.
 */

public class EnemyObj : KillableObj
{
    //How far can find the player
    public int distanceToSeePlayer = 5;
    //How close it need to be to attack
    public int distanceToAttack = 1;

    public float strGrowth = 1f;
    public float defGrowth = 1f;
    public float luckGrowth = 1f;
    public float spdGrowth = 0f;
    public float HPGrowth = 10f;
    public float expGrowth = 1f;

    public int dropLevelOffset = 0;
    //Audios that this NPC will play
    public NPCAudioManager npcAudio;
    public List<GameObject> Loots;
   public override void Start()
    {
        base.Start();
        
        //StartCoroutine(MovementTest());
    }
    public void SetLevel(int levelOffset)
    {
        
        if (levelOffset == 0)
            return;
        status.currLevel += levelOffset;
        status.strengh += levelOffset * strGrowth;
        status.defense += levelOffset * defGrowth;
        status.luck += levelOffset * luckGrowth;
        status.speed += levelOffset * spdGrowth;
        status.life += levelOffset * HPGrowth;
        status.currLife = status.life;
        status.GiveExpOnKill += levelOffset * expGrowth;
    }
    //Keep moving the NPC, for debugging
    public IEnumerator MovementTest()
    {
        Move(Sides.sideChoices.up);
        yield return new WaitForSeconds(1f);
        StartCoroutine(MovementTest());
    }
    //Can it see the player?
    public bool CanSeePlayer()
    {
        return DistanceFromObject(PlayerObj.playerInstance) <= distanceToSeePlayer /*&& !WallInPathToPlayer()*/;

    }
    //Closer direction to get to player
    public Sides.sideChoices DirectionToPlayer()
    {
        return DirectionToObj(PlayerObj.playerInstance);
    }
    //Can it attack the player?
    public bool CanAttackPlayer()
    {
        return DistanceFromObject(PlayerObj.playerInstance) <= distanceToAttack;
    }
    //Attack the player
    public void AttackPlayer()
    {
        AttackObj(PlayerObj.playerInstance, status.CalculateAttack());
    }

    //Move or attack the player
    public void Movement()
    {
        if (CanAttackPlayer())
        {
            //Debug.Log("Atacking Player");
            AttackPlayer();
        }
        else if (CanSeePlayer())
        {
            Move(DirectionToPlayer());
        }
        else
            Move(Sides.RandomSide());
    }

    //Move the enemy
    public override void Move(Sides.sideChoices side)
    {
        base.Move(side);

        InteractiveObj item = ObjectManager.GetInteractive(Position());


        //If there is a interactive on the ground, call its EnemyGet Function
        if (item != null)
        {
            item.EnemyGet();
        }
    }

    void Reset()
    {
        ObjType = Type.enemy;
    }
    public override void Die()
    {
        Debug.Log("Loot Dropped: " + Loots.Count);
        foreach (GameObject go in Loots)
        {
            //Instantiate(ObjectPrefabs.instance.OnObjectDeath, transform.position, Quaternion.identity);
            List<BaseObj> objs;
            int currTry = 0;
            do
            {
                //Choose a random X and Z position to create the weapon
                int RandomX = Random.Range(-1,2);
                int RandomZ = Random.Range(-1,2);
                //Get objects in this position
                objs = ObjectManager.GetObjectsAt(new IntVector2(Position().x+RandomX, Position().z+RandomZ));

                //Only create the weapon if there is no objects on this spot.
                if (objs.Count == 0)
                {
                    GameObject wDrop = Instantiate(go, new Vector3(Position().x+RandomX, 0, Position().z+RandomZ), Quaternion.identity,GameObject.Find("DungeonGenerator").transform);
                    if(wDrop.GetComponent<ItemWeaponObj>() != null && dropLevelOffset !=0)
                    {
                        wDrop.GetComponent<ItemWeaponObj>().weaponData.ApplyLevelOffset(dropLevelOffset);
                    }
                    //Debug.Log("Weapont at: " + RandomX + " " + RandomZ);
                }
                //Add one more try to create this weapon
                currTry++;

                //Do this while the weapon was not created of it got to the limit of tries to create it.
            } while (objs.Count != 0 && currTry <8);
            Debug.Log(go.name + "Spawned"+go.transform.position);
        }
        //Debug.Log("Weapont at: " + RandomX + " " + RandomZ);            
        base.Die();
    }
    //Called on his turn
    public override void OnStep()
    {

        base.OnStep();
        //For each action it have, do a action
        while (numberActions >= 1)
        {
            Movement();
            numberActions--;
        }
    }


}
