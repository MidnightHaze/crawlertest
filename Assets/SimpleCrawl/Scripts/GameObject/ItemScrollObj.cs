﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScrollObj : InteractiveObj
{
    public ScrollBase scrollData;
    public override void Start()
    {
        base.Start();
    }

    //Once the player get it
    public override void PlayerGet()
    {
        base.PlayerGet();
        if (PlayerObj.playerInstance.SetScroll(scrollData))
        {
            Destroy(gameObject);
        }

        //Give the weapon on the ground to the player
        

        //Set this weapon to be the one the player was using
        //weaponData = data;
        //Set the new texture for the weapon on the ground.

    }
}