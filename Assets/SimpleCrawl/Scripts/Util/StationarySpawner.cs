﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Spawn a random GameObject from a spawn pool.
 * It can be set to only have a chance to spawn
 */
public class StationarySpawner : SpawnChance
{

    //Objects it can spawn
    public GameObject[] SpawnPool;
    protected override void Awake()
    {
        base.Awake();
        //Only spawn the object if the chance to spawn is valid
        
            if (isSpawned)
            {
                GameObject chosen = SpawnPool[Random.Range(0, SpawnPool.Length)];
                Instantiate(chosen, transform.position, chosen.transform.rotation, transform.parent);

            }
       
    }
}
