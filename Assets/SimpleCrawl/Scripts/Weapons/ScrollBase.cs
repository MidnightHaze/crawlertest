﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScrollBase
{    
    public Sprite Image;
    public int StackCount;

    public string Name;
    public string Tooltip;

    public void InitCount(int stack)
    {
        StackCount = stack;
    }
    public bool UseStack()
    {
        StackCount--;
        if (StackCount <= 0)
            return false;
        return true;
    }

    public KillableObj[] Hit(IntVector2 pos, Sides.sideChoices attackSide)
    {
        List<KillableObj> list = new List<KillableObj>();
        List<IntVector2> attackPos = new List<IntVector2>();

        for (int i = 1; i < 5; i++)
        {
            attackPos.Add(pos + new IntVector2(Sides.SideToVector(Sides.sideChoices.up).x*i, Sides.SideToVector(Sides.sideChoices.up).z*i));
            attackPos.Add(pos + new IntVector2(Sides.SideToVector(Sides.sideChoices.down).x * i, Sides.SideToVector(Sides.sideChoices.down).z * i));
            attackPos.Add(pos + new IntVector2(Sides.SideToVector(Sides.sideChoices.left).x * i, Sides.SideToVector(Sides.sideChoices.left).z * i));
            attackPos.Add(pos + new IntVector2(Sides.SideToVector(Sides.sideChoices.right).x * i, Sides.SideToVector(Sides.sideChoices.right).z * i));
            ;
        }


        //타격 계산
        List<KillableObj> tempList = new List<KillableObj>();
        List<Vector3> hitPos = new List<Vector3>();
        List<Vector3> missPos = new List<Vector3>();

        for (int i = 0; i < attackPos.Count; i++)
        {
            tempList = ObjectManager.GetObjectsAt<KillableObj>(attackPos[i]);
            if (tempList.Count != 0)
            {
                hitPos.Add(new Vector3(attackPos[i].x, 1, attackPos[i].z));

                list.AddRange(tempList);
            }
            else
            {
                missPos.Add(new Vector3(attackPos[i].x, 1, attackPos[i].z));

            }
            tempList.Clear();
        }
        
        for (int i = 0; i < hitPos.Count; i++)
        {
            GameObject.Instantiate(ObjectPrefabs.instance.OnAttackHit, hitPos[i], ObjectPrefabs.instance.OnAttackHit.transform.rotation);
        }
        for (int i = 0; i < missPos.Count; i++)
        {
            GameObject.Instantiate(ObjectPrefabs.instance.OnAttackMiss, missPos[i], ObjectPrefabs.instance.OnAttackHit.transform.rotation);
        }
        
        return list.ToArray();
    }

    public float GetAttack()
    {
        return 55f;
    }

}
